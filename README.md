# HashCode 2020(noRagrets)

<b>Problem statement</b> - root folder <br>
<b>Official input files</b> - ./input <br>
<b>Generated output files</b> - ./output <br>
<b>File which runs the solution</b> - ./main.cpp <br>
<b>Folder which contains the solutions</b> - ./solutions <br>

## Steps to run the program

---

1. Compile and run main.cpp
2. Select input file name

    - a - a_name.in
    - b - b_name.in
    - c - c_name.in
    - d - d_name.in
    - e - e_name.in

3. Select the folder you want to read the files from

    - 0 - default location
    - any other string will be considered as path

4. Select the folder you want to store the files in

    - rules same as input

---

### Important Notes

> - Create a new branch and make the changes there. After you're confident with your code upload it to your branch <br>
> - Everybody doesn't necessarily create their own branch. Two members working on the same solution share the same branch.<br>
> - When we decide that a branch's code is the best one we have, we merge it into master.
> - When two or more member are working on the same branch, use push to upload your commit and the other members use fetch to get the latest changes without affecting your work.

---

> These options are given so that different team members can try different things and not block other members in their work. So bhai log use the options well.

---

> Please use vscode and C/C++ extension for maintaining the same lint style for consistency.
