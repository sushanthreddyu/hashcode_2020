#include <bits/stdc++.h>

#include "classes.h"

#define INPUT_FOLDER "./input/"
#define OUTPUT_FOLDER "./output/"

using namespace std;

string getFileName(char file)
{
    string name;
    switch (file)
    {
    case 'a':
        name = "a_example";
        break;
    case 'b':
        name = "b_read_on";
        break;
    case 'c':
        name = "c_incunabula";
        break;
    case 'd':
        name = "d_tough_choices";
        break;
    case 'e':
        name = "e_so_many_books";
        break;
    case 'f':
        name = "f_libraries_of_the_world";
        break;
    }

    return name;
}

signed main()
{
    char input_file;
    string input_folder;
    string output_folder;
    cout << "Select input file" << endl;
    cin >> input_file;
    cout << "Select input folder" << endl;
    cin >> input_folder;
    cout << "Select output folder" << endl;
    cin >> output_folder;

    if (input_folder == "0")
        input_folder = INPUT_FOLDER;
    if (output_folder == "0")
        output_folder = OUTPUT_FOLDER;

    string file_name = getFileName(input_file);

    // The path where we want to get out input from
    string input_path = input_folder + file_name + ".txt";

    // TODO : call solution functions

    signup_priority temp(input_path);

    temp.getOutput();
    //  The path where we want to store our output in
    string output_path = output_folder + file_name + "_out.txt";

    // TODO : need to change after looking at Question

    ofstream output;
    output.open(output_path, ios::out);
}