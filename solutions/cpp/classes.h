#include <bits/stdc++.h>

#define pb push_back
#define int long long
#define lib_vect vector<vector<int>, vector<int>>

using namespace std;

class signup_priority
{
private:
    int no_of_books, no_of_libraries, no_of_days;
    vector<int> books;
    map<int, lib_vect> libraries;

public:
    signup_priority(string input_path);
    ~signup_priority();
    void getOutput();
};