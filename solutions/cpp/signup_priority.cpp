#include <bits/stdc++.h>
#define pb push_back
#define int long long
#define lib_vect vector<vector<int>, vector<int>>

using namespace std;

class signup_priority
{
private:
    /* data */
    int no_of_books, no_of_libraries, no_of_days;
    vector<int> books;
    map<int, lib_vect> libraries;

public:
    signup_priority(string input_path)
    {
        fstream input;

        input.open(input_path);

        input >> no_of_books;
        input >> no_of_libraries;
        input >> no_of_days;

        for (int i = 0; i < no_of_books; i++)
        {
            int temp;
            input >> temp;
            books.pb(temp);
        }
        for (int i = 0; i < no_of_libraries; i++)
        {
            int books_lib, days_lib, books_per_day;
            input >> books_lib;
            input >> days_lib;
            input >> books_per_day;
            vector<int> vals = {books_lib, days_lib, books_per_day};
            vector<int> book_ids;
            for (int j = 0; j < books_lib; j++)
            {
                int temp;
                input >> temp;
                book_ids.pb(temp);
            }
            lib_vect lib_temp;
            lib_temp.pb(vals);
            lib_temp.pb(book_ids);
            libraries[i] = lib_temp;
        }
    }
    ~signup_priority();
    void getOutput()
    {
        for (auto i : libraries)
        {
            vector<int> temp1 = i.second[0];
            vector<int> temp2 = i.second[1];
            for (auto j : temp1)
                cout << j << " ";
            for (auto j : temp2)
                cout << j << " ";
        }
    }
};
